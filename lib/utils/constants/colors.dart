import 'package:flutter/material.dart';

const Color blackColor = Colors.black;

const Color whiteColor = Colors.white;

const Color pinkColor = Color(0xFFbf4988);

const Color pinkAccentColor = Color(0xFFC2478A);

const Color pinkAccentColor2 = Color(0xFFCA528D);

const Color lightPink = Color(0xFFFD939F);

const Color yellow = Color(0xFFfed835);

const Color coral = Color(0xFFFF5D5D);

const Color lightYellow = Color(0xFFFFFEE1);

const Color violet = Color(0xFF2F2564);

const Color darkPurple = Color(0xFF3E2968);

const Color purple = Color(0xFF86397B);

const Color purpleAccent = Color(0xFFC44FEE);

const Color blueAccentColor = Color(0xFF1473E6);

const Color blue1 = Color(0xFF0D2576);

const Color blue2 = Color(0xFF01164D);

const Color moodGrey = Color(0xFFD9D9D9);

const Color normalGrey = Colors.grey;

const Color darkGrey = Color(0xFF3B3A3A);

const Color mediumGrey = Color(0xFF464646);

const Color lightGrey = Color(0xFF5C5C5C);

const Color borderGrey = Color(0xFF707070);

const Color dividerGrey = Color(0xFFCECECE);

const Color fieldPink = Color(0xFFFBBECE);

const Color bgPink = Color(0xFFFDE7E9);

const Color textLightGrey = Color(0xFFA9A9A9);

const Color orangeIconColor = Color(0xFFFC2222);

const Color blueCrossIcon = Colors.blue;

const Color inkwellFocusColor = Colors.blue;

LinearGradient pinkOrangeGrad = const LinearGradient(
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
  colors: [pinkColor, pinkColor, orangeIconColor],
);

LinearGradient gradient2 = const LinearGradient(
  colors: [
    Color(0xFF2f2763),
    pinkColor,
  ],
  begin: Alignment.topCenter,
  end: Alignment.bottomRight,
);

LinearGradient screenGradient = const LinearGradient(
  colors: [
    pinkAccentColor,
    violet
  ],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  stops: [0, 0.76],
);

